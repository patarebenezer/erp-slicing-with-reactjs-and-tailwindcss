function question1(n) {
    const s = 'Question 1: '
    if (n % 15 === 0) console.log(s, n + ' = Fish Bash');
    else if (n % 5 === 0) console.log(s, n + ' = Bash');
    else if (n % 3 === 0) console.log(s, n + ' = Fish');
    else console.log(s, 'Number out of condition')
}

async function question2(arr) {
    const sorted = [];
    const subPromises = arr.map(elem => {
        return new Promise((rs, rj) => {
            setTimeout(() => {
                sorted.push(elem);
                rs();
            }, elem * 100);
        });
    });
    await Promise.all(subPromises);
    console.log('Question 2: ', sorted);;
}

function question3(str1, str2) {
    let reverse = str2.split("").reverse().join("")
    const cek = str1 === reverse ? 'TRUE' : 'FALSE'
    setTimeout(() => {
        console.log('Question 3: ',cek);
    },5000)
}

question1(15)
question2([10, 28, 4, 0, 30, 40])
question3('patar', 'ratap')
