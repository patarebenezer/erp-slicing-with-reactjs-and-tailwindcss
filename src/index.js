import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './components/App';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './components/Login';
import NotFound from './components/NotFound';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <Routes>
            <Route path='/' element={<App />}/>
            <Route path='/about' element={<App />}/>
            <Route path='/price' element={<App />}/>
            <Route path='/contact' element={<App />}/>
            <Route path='/login' element={<Login />}/>
            <Route path='*' element={<NotFound/>}/>
        </Routes>
    </BrowserRouter>
);
