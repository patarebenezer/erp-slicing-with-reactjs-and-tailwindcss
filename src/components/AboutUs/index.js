import app from '../../assets/images/erp2.png'
import Button from '../atoms/Button'
const AboutUs = () => {
    return (
            <section className="wrapper">
                <div className='grid lg:grid-cols-2 items-center'>
                    <div className='mx-auto'>
                        <img src={app} alt="" className='' />
                    </div>
                    <div className='text-[46px] mt-8 lg:mt-0 text-dark font-bold'>
                        <span className="relative overflow-hidden">
                            About<span className="text__underline !h-2 !w-[95%]"></span>
                        </span>
                        <span> Us</span>

                        <p className='text-[18px] font-light text-gray-400'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio soluta voluptate, ipsa est dolorum similique molestias numquam voluptatibus, iure beatae a hic commodi? Dolorum consequatur ullam quod modi eos omnis!</p>
                        <p className='text-[18px] font-light text-gray-400 mt-4'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio soluta voluptate, ipsa est dolorum similique molestias numquam voluptatibus, iure beatae a hic commodi? Dolorum consequatur ullam quod modi eos omnis!</p>
                        <div className='flex text-center mt-6'>
                            <Button name={'Contact Us'} cls={'w-full lg:w-1/2 bg-cyan-400 !text-white text-[18px] py-2 px-3 hover:bg-cyan-100 hover:!text-cyan-400 font-light  hover:bg-cyan-50'} />
                        </div>
                    </div>
                </div>
            </section>
    )
}

export default AboutUs