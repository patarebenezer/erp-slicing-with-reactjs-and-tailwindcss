import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

const NotFound = () => {
    return (
        <div id="main">
            <div class="fof">
                <h1>Error 404</h1>
            </div>
            <Link to={'/'} className='absolute left-[49%] underline text-blue-500 bottom-[30%]'>Back</Link>
        </div>
    )
}

export default NotFound