import Button from "../atoms/Button"
import hero_image from '../../assets/images/erp.png'
import blob3 from '../../assets/svgs/blob3.png'

const Hero = () => {
    return (
        <section className="wrapper relative h-screen">
            <div className="grid lg:grid-cols-2 lg:px-9 justify-items-center items-center">
                <div className="lg:!mt-[-40px] mb-10 lg:m-0 n">
                    <div className="hidden lg:flex">
                        <img src={hero_image} alt="" className="w-full lg:w-10/12 putar" />
                    </div>
                    <div className="absolute right-0 bottom-0 -z-10">
                        <img src={blob3} alt="" className="lg:w-[800px]" />
                    </div>
                </div>
                <div className="mt-0 lg:ml-[-80px] !z-10 lg:mt-[-80px]">
                    <div className="text-[46px]">
                        <span className="relative overflow-hidden">
                            Hanya <span className="text__underline h-2 w-[90%]"></span>
                        </span>
                        700K/bulan <br className="hidden md:block" /> Untuk Semua&nbsp;
                        <span className="relative">
                            Fitur!<span className="text__underline h-2"></span>
                        </span>
                    </div>
                    <div className="mt-[10px] mb-10 text-xl text-gray-400 leading-[32px] font-light">
                        100% web basis ERP yang menyediakan<br className="hidden md:block" />
                        semua kebutuhan Anda.
                    </div>
                    <Button name={'Show Me'} cls={'bg-cyan-400 !text-white hover:bg-cyan-100 hover:!text-cyan-400 font-normal !px-8 !py-3 hover:bg-cyan-50'} />
                </div>
            </div>
        </section>
    )
}

export default Hero