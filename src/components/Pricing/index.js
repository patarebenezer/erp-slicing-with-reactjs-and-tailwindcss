import { GiCheckMark } from 'react-icons/gi'
import Button from '../atoms/Button'
import pricing from '../json/data.json'

const Pricing = () => {
    return (
        <section className='wrapper'>
            <h2 className='text-5xl uppercase py-8 text-center font-bold'>Pricing</h2>
            <div className='mt-4 grid lg:grid-cols-2 gap-4'>
                <div className='px-8 py-6 text-center lg:text-left '>
                    <h1 className='text-3xl font-bold mb-2'>Select your plan</h1>
                    <p className='font-light text-gray-400'>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis aspernatur consequuntur eos a illo iusto ratione modi magni ipsam saepe sapiente, provident quia autem quis aut quibusdam dolorum enim accusantium.</p>
                </div>
                <div className='pb-8'>
                    {
                        pricing.map((data, i) => {
                            return <div className='w-full lg:w-10/12 px-4 mb-4 py-4 border-2 hover:border-cyan-300 rounded-md'>
                                <input id={`radio${i}`} type="radio" name="radio" class="hidden" />
                                <label for={`radio${i}`} class="flex items-center cursor-pointer mb-3">
                                    <span class="w-6 h-6 inline-block mr-1 rounded-full border border-grey"></span>
                                    <div className='w-full grid grid-cols-2 gap-4 justify-evenly'>
                                        <p className='px-3 text-xl'>{data.title}</p>
                                        <p className='px-3 text-xl text-right'>{data.amount}</p>
                                    </div>
                                </label>
                                {data.detail.map(details => (
                                    <div className="flex gap-3 px-10 items-center">
                                        <GiCheckMark />
                                        <p className='font-light text-gray-500'>{details}</p>
                                    </div>
                                ))}

                            </div>
                        })
                    }
                    <div className='flex text-center mt-6 lg:mt-0'>
                        <Button name={'Continue'} cls={'w-full lg:w-10/12 bg-cyan-400 !text-white text-[18px] py-2 px-3 hover:bg-cyan-100 hover:!text-cyan-400 font-light  hover:bg-cyan-50'} />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Pricing
