import logo from '../../assets/svgs/logo2.png'
import blob from '../../assets/svgs/blob2.png'
import { useState } from 'react'
import { GrClose } from 'react-icons/gr'
import Menu from './Menu'
import { Link } from 'react-router-dom'
import Svg from '../Login/Svg'

const Header = () => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <>
            <div className={`${isOpen ? 'block' : 'hidden'} h-screen fixed z-20 top-0 right-0 bg-slate-50 shadow-2xl w-[60%]`}>
                <div className='flex pt-12 pl-6 pr-8 justify-evenly'>
                    <p className='text-xl w-full'>ERP System</p>
                    <GrClose className='w-6 h-6 cursor-pointer' onClick={() => setIsOpen(!isOpen)} />
                </div>
                <div className='grid px-6 py-4 gap-6 items-center'>
                    <Menu open={isOpen}/>
                </div>
            </div>
            <div className={`wrapper mt-[30px]`}>
                <div className='flex items-center justify-between'>
                    <div className='flex w-full lg:w-auto justify-between'>
                        <div className='z-[-1] absolute top-[-100px] left-0 lg:top-[-60px] lg:left-[-40px]'>
                            <img src={blob} alt="" className='w-[300px] rotate-0 lg:rotate-12 lg:w-[340px]' />
                        </div>
                        <div className='flex items-center gap-4'>
                            <Svg cls={'text-white'}/>
                            <Link to='/' className='text-xl tracking-wider text-white font-bold'>Home</Link>
                        </div>
                        <button onClick={() => setIsOpen(!isOpen)} className="block p-1 outline-none lg:hidden mobile-menu-button" data-target="#navigation">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-7 h-7 text-dark-1 dark:text-light-1"
                                x-show="!showMenu" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                    d="M4 6h16M4 12h16M4 18h16"></path>
                            </svg>
                        </button>
                    </div>

                    <div className='hidden lg:flex gap-10 items-center'>
                        <Menu open={isOpen}/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Header