import React from 'react'
import Button from '../atoms/Button'
import { Link, useLocation } from 'react-router-dom'
import { useState } from 'react'

const Menu = () => {
    const location = useLocation()
    const path = location.pathname
    const activeLink = 'text-cyan-400'
    const normalLink = 'font-light text-gray-400 hover:text-black'
    return (
        <>
            <Link to={'/about'} className={path !== '/about' ? normalLink : activeLink}>About</Link>
            <Link to={'/price'} className={path !== '/price' ? normalLink : activeLink}>Pricing</Link>
            <Link to={'/contact'} className={path !== '/contact' ? normalLink : activeLink}>Contact</Link>
            <Button name={'Login'} links='/login' cls={'text-md w-1/2'} />
        </>
    )
}

export default Menu