import food from '../../assets/svgs/logo.png'
const Footer = () => {
    return (
        <>
        <section className="wrapper flex flex-col lg:flex-row justify-center lg:items-center gap-5 lg:gap-[120px] leading-8 mb-5 lg:mb-20">
            <div className="max-w-[200px]">
                <img src={food} alt="food" />
            </div>
            <div className='flex flex-col'>
                <div className='font-semibold mb-3'>
                    Portfolio
                </div>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">Container Loader Program</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">AXA Smart Drive</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">WayFinder</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">Real Time Chatbot</a>
            </div>
            
            <div className='flex flex-col'>
                <div className='font-semibold mb-3'>
                    Company
                </div>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">APIs Developer</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">Career</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">Term & Conditions</a>
                <a className='font-light text-gray-400 hover:underline hover:text-black' href="#">Privacy Policy</a>
            </div>

            <div className='flex flex-col'>
                <div className='font-semibold mb-3'>
                    Contact
                </div>
                <p className='font-light text-gray-400 hover:underline'> Medan, Sumatera Utara</p>
                <p className='font-light text-gray-400 hover:underline'> patarebenezer05@gmail.com</p>
                <p className='font-light text-gray-400 hover:underline'> 0812 5055 914</p>
                <p>&nbsp;</p>
                
            </div>

            
        </section>
        
        <section className='wrapper text-[13px] lg:text-base text-gray-400 tracking-wide font-light text-center border-t !pt-5'>
                Created by Patar Ebenezer Siahaan 2022 (Keda Tech Test)
            </section>
        </>
    )
}

export default Footer