import Footer from "./Footer"
import Header from "./Header"
import Hero from "./Hero"
import AboutUs from "./AboutUs"
import Special from "./Special"
import Pricing from "./Pricing"
import Testimonial from "./Testimonial"

const App = () => {
  return (
    <div className="mb-5">
      <Header/>
      <Hero />
      <Special />
      <Pricing/>
      <Testimonial />
      <AboutUs />
      <Footer />
    </div>
  )
}

export default App