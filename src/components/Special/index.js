import { Link } from "react-router-dom"
import data from '../json/data.json'
import { FiCheckCircle } from 'react-icons/fi'
import Button from "../atoms/Button"
const Special = () => {
  return (
    <section className="wrapper leading-[40px]">
      <div className="text-center">
        <p className="font-light text-gray-400">Produk Kategori</p>
        <h2 className="text-2xl">Pilih Paket Sesuai Kebutuhan Anda</h2>
        <div className="menu flex justify-center font-light gap-8 mt-4">
          <Link to={'/'} className='nav-link2 active2'>All <span className="text__underline2"></span></Link>
          <Link to={'/'}>Basic</Link>
          <Link to={'/'}>Business</Link>
          <Link to={'/'}>Entrepreneur</Link>
        </div>
      </div>
      <br />
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:flex lg:flex-row justify-center gap-8">
        {data.map(item => (
          <div key={item} className="bg-gray-100 relative flex flex-col w-full lg:w-11/12 py-8 rounded-xl">
            <div className="grid grid-cols-2 text-center">
              <p className="flex gap-1 ml-4">
                {
                  [...Array(item.star)].map((x) => {
                    return <img src={require('../../assets/svgs/star.svg').default} className="w-[18px]" alt={x} />
                  })
                }
              </p>
              <p className={`rounded-bl-lg rounded-tl-lg font-bold py-1 ${item.title === 'Basic' ? 'bg-gray-200' : 'bg-yellow-400'} ${item.title === 'Entrepreneur' ? 'bg-red-500 text-white' : ''}`}>{item.title}</p>
            </div>

            <div className="mt-8 mb-20">
              {item.detail.map(details => (
                <div className="flex justify-between px-4 items-center">
                  <p>{details}</p>
                  <FiCheckCircle/>
                </div>
              ))}
            </div>
            <div className="w-full text-center absolute bottom-0 mb-8">
            <Button name={'Select Package'} cls={'bg-cyan-400 !text-white hover:bg-cyan-100 hover:!text-cyan-400 font-normal !px-8 !py-3 hover:bg-cyan-50'} />
              
            </div>
          </div>
          
        ))}
      </div>

    </section>
  )

}

export default Special