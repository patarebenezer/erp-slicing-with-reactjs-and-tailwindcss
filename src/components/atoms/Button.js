import { Link } from "react-router-dom"

const Button = ({cls, name,links}) => {
  return (
     <Link to={`${links}`} className={`${cls} rounded-lg px-10 py-1 hover:bg-cyan-50 text-cyan-400 outline-cyan-400 outline outline-offset-2 outline-2`}>{name}</Link>   
  )
}

export default Button