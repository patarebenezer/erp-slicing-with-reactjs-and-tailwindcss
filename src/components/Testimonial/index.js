import testi_img from '../../assets/images/avatar.png'
import kutip from '../../assets/svgs/quote1.svg'

const Testimonial = () => {
    return (
        <section className="wrapper flex justify-center">
            <div className='max-w-[768px] grid justify-items-center gap-y-10'>
                <div className='relative'>
                <img src={kutip} alt="kutip" className='absolute top-10 lg:top-4 left-[-5%] lg:left-[-12%] z-[-10]' />
                    <p className='text-2xl lg:text-4xl mt-20 text-gray-700 font-light leading-[34px] lg:leading-[55px] text-center'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus facere libero quidem repellat ipsam alias quam nihil officiis tempore, repudiandae provident! </p>
                </div>
                <div className='flex items-center gap-6'>
                    <img src={testi_img} alt="testi"
                        className='w-24 rounded-full object-cover border-b-purple-200 border-r-purple-200 border-r-4 border-b-4 border-l-yellow-100 border-t-yellow-100 border-t-2 border-l-2' />
                    <div>
                        <h2 className='text-xl'>Camella Sarrah</h2>
                        <p className='text-gray-400 font-light'>Client</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Testimonial